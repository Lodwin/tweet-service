package com.manage.tweet.service;

import com.manage.tweet.exceptionManager.CustomExceptionMessages;
import com.manage.tweet.exceptionManager.NoContentExceptionHandler;
import com.manage.tweet.exceptionManager.ValidationHandler;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.NotDirectoryException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Service @Getter
public class TweetService implements ITweetService {
    private static final Logger log = LoggerFactory.getLogger(TweetService.class);
    private final HashMap<String, List<String>> followers = new HashMap<>();
    private final HashMap<String, List<String>> tweets = new HashMap<>();
    private final ArrayList<String> users = new ArrayList<>();

    /**
     * Accepts two files for processing
     * Is an implementation of processFiles method in the ITweetService
     */
    @Override
    public void processFiles(String userFile, String tweetFile) throws Exception {
        if (StringUtils.isBlank(userFile) || StringUtils.isBlank(tweetFile)) {
            throw new ValidationHandler(CustomExceptionMessages.VALIDATION);
        }
        processUsersFile(userFile);
        processTweetFile(tweetFile);
    }

    /**
     * The method processes user file contents
     * create users and followers map
     */
    public void processUsersFile(String userFile) throws NotDirectoryException {
        InputStream inputStream = validateInputFile(userFile);
        try {
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(inputStream));
            String line, follower, key;
            while ((line = bufferedReader.readLine()) != null) {
                String[] usersArray = line.split(" follows |, ");
                follower = usersArray[0];
                if (!users.contains(follower)) {
                    users.add(follower);
                }
                for (int i = 1; i < usersArray.length; i++) {
                    key = usersArray[i];
                    if (followers.containsKey(key)) {
                        List<String> existingList = followers.get(key);
                        //add follower only it's not already on the list
                        if (!existingList.contains(follower)) {
                            existingList.add(follower);
                        }
                        followers.put(key, existingList);
                    } else {
                        //list of users the user is following
                        List<String> followerList = new ArrayList<>();
                        followerList.add(follower);
                        followers.put(key, followerList);
                    }
                    if (!users.contains(key)) {
                        users.add(key);
                    }
                }
            }
            if (users.isEmpty()){
                throw new NoContentExceptionHandler();
            }
        } catch (Exception exception) {
            log.error(exception.getMessage());
            throw new InternalError(CustomExceptionMessages.INTERNAL_ERROR);
        }
    }

    /**
     * The method processes user file contents
     * map tweets to users
     */
    public void processTweetFile(String tweetFile) throws NotDirectoryException {
        InputStream inputStream = validateInputFile(tweetFile);
        try {
            BufferedReader bufferedReader =
                    new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] tweetsArray = line.split(">");
                if (tweetsArray.length >= 2) {
                    String user = tweetsArray[0];
                    String tweet = tweetsArray[1];
                    //map tweet to the user
                    mapTweetToUser(user, user, tweet, false);
                    List<String> userFollowers = followers.get(user);
                    if (userFollowers != null) {
                        for (String follower : userFollowers) {
                            //map tweet to the user's followers
                            mapTweetToUser(follower, user, tweet, true);
                        }
                    }
                }
            }
            if (tweets.isEmpty()){
                throw new NoContentExceptionHandler();
            }
            printOutput();
        } catch (Exception exception) {
            log.error(exception.getMessage());
            throw new InternalError(CustomExceptionMessages.INTERNAL_ERROR);
        }
    }

    /**
     * This method maps tweets to users [And followers]
     */
    public void mapTweetToUser(
            String key, String user,
            String tweet, boolean addFollower) {
        if (tweets.containsKey(key)) {
            List<String> userTweets = tweets.get(key);
            userTweets.add(concatUserAndTweet(user, tweet));
            if (addFollower) {
                //add tweet to the user's followers
                tweets.put(key, userTweets);
            } else {
                //add tweet to the user
                tweets.put(user, userTweets);
            }
        } else {
            List<String> followerTweets = new ArrayList<>();
            followerTweets.add(concatUserAndTweet(user, tweet));
            tweets.put(key, followerTweets);
        }
    }

    /**
     * Concat user and tweet to the required format
     */
    public String concatUserAndTweet(String user, String tweet) {
        return "@" + user + ": " + tweet;
    }

    public InputStream validateInputFile(String file) throws NotDirectoryException {
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(file);
        } catch (Exception exception) {
            throw new NotDirectoryException(CustomExceptionMessages.DIRECTORY_NOT_FOUND);
        }
        return inputStream;
    }

    /**
     * This method prints Users and their tweets in descending order
     */
    private void printOutput() {
        Collections.sort(users);
        for (String key : users) {
            System.out.println("\n" + key);
            if (tweets.containsKey(key)) {
                tweets.get(key).forEach(System.out::println);
            } else {
                System.out.println();
            }
        }
    }
}
