package com.manage.tweet.service;

public interface ITweetService {
    void processFiles(String userFile, String tweetFile) throws Exception;
}
