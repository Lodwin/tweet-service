package com.manage.tweet.exceptionManager;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.nio.file.NotDirectoryException;

@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Exception Handler responsible for service illegal exceptions
     */
    @ExceptionHandler(value = {IllegalArgumentException.class, IllegalStateException.class, NotDirectoryException.class})
    protected ResponseEntity<Object> handleConflict(Exception exception, WebRequest request) {
        BaseExceptionsResponse error = BaseExceptionsResponse.of(
                HttpStatus.BAD_REQUEST.name(),
                CustomExceptionMessages.DIRECTORY_NOT_FOUND);
        HttpHeaders headers = withHeaders();
        return handleExceptionInternal(exception, error, headers, HttpStatus.BAD_REQUEST, request);
    }

    /**
     * Exception Handler responsible for service illegal exceptions
     */
    @ExceptionHandler(value = InternalError.class)
    protected ResponseEntity<Object> internalError(Exception exception, WebRequest request) {
        BaseExceptionsResponse error = BaseExceptionsResponse.of(
                HttpStatus.BAD_REQUEST.name(),
                CustomExceptionMessages.INTERNAL_ERROR);
        HttpHeaders headers = withHeaders();
        return handleExceptionInternal(exception, error, headers, HttpStatus.BAD_REQUEST, request);
    }

    /**
     * Exception Handler responsible for validating player information at creation.
     */
    @ExceptionHandler(value = ValidationHandler.class)
    protected ResponseEntity<Object> validation(Exception exception, WebRequest request) {
        BaseExceptionsResponse error = BaseExceptionsResponse.of(
                HttpStatus.BAD_REQUEST.name(),
                CustomExceptionMessages.VALIDATION);
        HttpHeaders headers = withHeaders();
        return handleExceptionInternal(exception, error, headers, HttpStatus.BAD_REQUEST, request);
    }

    /**
     * Exception Handler responsible for service not found exceptions
     */
    @ExceptionHandler(value = NoContentExceptionHandler.class)
    protected ResponseEntity<Object> notFoundExceptions(Exception exception, WebRequest request) {
        BaseExceptionsResponse error = BaseExceptionsResponse.of(
                HttpStatus.NO_CONTENT.name(),
                CustomExceptionMessages.NO_CONTENT);
        HttpHeaders headers = withHeaders();
        return handleExceptionInternal(exception, error, headers, HttpStatus.NO_CONTENT, request);
    }

    private static HttpHeaders withHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
