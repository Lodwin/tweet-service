package com.manage.tweet.exceptionManager;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NO_CONTENT, reason = CustomExceptionMessages.NO_CONTENT)
public class NoContentExceptionHandler extends RuntimeException {}
