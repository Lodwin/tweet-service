package com.manage.tweet.exceptionManager;

public interface CustomExceptionMessages {
    String NO_CONTENT = "Empty File";
    String DIRECTORY_NOT_FOUND = "Incorrect file directory, please verify";
    String INTERNAL_ERROR = "Error occurred while processing, please see error description on " +
            "console logs";
    String VALIDATION = "User and Tweet files are required";
}
