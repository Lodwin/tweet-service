package com.manage.tweet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * Entry point into the tweet application.
 */
@SpringBootApplication
public class TweetApplication {
    public static void main(String[] args){
        SpringApplication.run(TweetApplication.class, args);
    }
}
