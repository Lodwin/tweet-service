package com.manage.tweet.resource;

import com.manage.tweet.dto.FileDto;
import com.manage.tweet.service.ITweetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Tweet service endpoints
 */
@RestController
@RequestMapping("/tweet")
public class TweetResource {
    @Autowired
    private ITweetService tweetService;

    @PostMapping()
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String processTweets(@RequestBody FileDto fileDto) throws Exception {
        tweetService.processFiles(fileDto.getUserFile(), fileDto.getTweetFile());
        return "Please see results on console";
    }
}
