package com.manage.tweet.dto;

import lombok.Data;
/**
 * Lombok to specify that this is a data class.
 * It is mainly going to be used as a REST Data Model.
 */
@Data
public class FileDto {
    private String userFile;
    private String tweetFile;
}
