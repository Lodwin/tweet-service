package com.manage.tweet.service;

import com.manage.tweet.exceptionManager.CustomExceptionMessages;
import com.manage.tweet.exceptionManager.NoContentExceptionHandler;
import com.manage.tweet.exceptionManager.ValidationHandler;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.file.NotDirectoryException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ServiceTests {
    @Autowired
    private TweetService tweetService;
    public static final String userFilePath = "src/main/resources/sample/user.txt";
    public static final String wrongUserPath = "src/main/resources/sample/use.txt";
    public static final String tweetFilePath = "src/main/resources/sample/tweet.txt";
    public static final String Alan = "Alan";
    public static final String Martin = "Martin";
    public static final String Ward =  "Ward";

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void processUserFile() throws NotDirectoryException {
        tweetService.processUsersFile(userFilePath);
        Assert.assertTrue(tweetService.getUsers().size() > 1);
        Assert.assertTrue(tweetService.getFollowers().size() > 1);
    }

    @Test
    public void processTweetFile() throws NotDirectoryException {
        tweetService.processTweetFile(tweetFilePath);
        Assert.assertTrue(tweetService.getTweets().size() > 1);
    }

    @Test
    public void processFiles() throws Exception {
        tweetService.processFiles(userFilePath, tweetFilePath);
        Assert.assertTrue(tweetService.getUsers().size() > 1);
        Assert.assertTrue(tweetService.getFollowers().size() > 1);
        Assert.assertTrue(tweetService.getTweets().size() > 1);
        Assert.assertTrue(tweetService.getUsers().containsAll(new ArrayList<>(Arrays.asList(
                Alan, Martin, Ward))));
        if (tweetService.getFollowers().containsKey(Ward)) {
            List<String> followers = tweetService.getFollowers().get(Alan);
            Assert.assertEquals(followers.get(0), Ward);
        }
        if (tweetService.getTweets().containsKey(Alan)) {
            List<String> tweets = tweetService.getTweets().get(Alan);
            Assert.assertEquals(
                    tweets.get(0),
                    "@Alan:  If you have a procedure with 10 parameters, " +
                            "you probably missed some.");
        }
        if (tweetService.getTweets().containsKey(Ward)) {
            List<String> tweets = tweetService.getTweets().get(Ward);
            Assert.assertEquals(
                    tweets.get(1),
                    "@Ward:  There are only two hard things in Computer Science:" +
                            " cache invalidation, naming things and off-by-1 errors.");
        }
    }

    @Test(expected = NoContentExceptionHandler.class)
    public void processFiles_noContent() throws Exception {
        tweetService.processFiles(userFilePath, tweetFilePath);
    }

    @Test
    public void processFiles_validateFileNonNull() throws Exception {
        exceptionRule.expect(ValidationHandler.class);
        exceptionRule.expectMessage(CustomExceptionMessages.VALIDATION);
        tweetService.processFiles("", tweetFilePath);
    }

    @Test
    public void processFiles_wrongPath() throws Exception {
        exceptionRule.expect(NotDirectoryException.class);
        exceptionRule.expectMessage(CustomExceptionMessages.DIRECTORY_NOT_FOUND);
        tweetService.processFiles(wrongUserPath, userFilePath);
    }
}
