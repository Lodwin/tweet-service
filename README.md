# Tweet Service
Tweet Service is a service that prints on console user, followers, and their tweets
# Build Tool
- ##### Maven
# DevOps
- ##### Bitbucket
- ##### Git
# Frameworks
#### Spring Boot
- Service build framework.
- Service Endpoint Url (`http://localhost:8080/tweet`).
#### Spring Actuator
- Used for service **monitoring**.
    - All components - Url (`http://localhost:8080/manage-tweet-service`).
    - Specific components - Url (`http://localhost:8080/manage-tweet-service/*`).
        - ###### Health check
            - Url (`*/health`)
        - ###### Information
            - Url (`*/info`)
        - ###### Metrics
            - Url (`*/metrics`)
#### Spring Controller Advice
- Used for exception handling strategy.
#### Lombok
- Java Library used for reducing boilerplate code such as getters and setters etc.
# Run Instructions
- Clone service
    - Url (`git clone https://Lodwin@bitbucket.org/Lodwin/tweet-service.git`)
- Run maven refresh to import all dependencies.
- Run maven [clean, install] goals to build service and jar.
- Run the service either from IDE or terminal by executing the run-script.
- The service will be exposed on port `8080`.
# Run With Script
- Execute script (`./run-script.sh`) that will execute tweet-service jar
# Testing
- Use tools such as `Insomnia` or `Swagger Inspector` to consume the service.
- Find sample payloads in the application `resources` directory or use the below examples.
- Endpoint `http://localhost:8080/tweet`.
- Example payload - HTTP POST
```json
  {
      "userFile": "src/main/resources/files/user.txt",
      "tweetFile": "src/main/resources/files/tweet.txt"
  }
```
# Future Improvements
- Service security layer implementation such that only authorized clients can consume the service.
    - ###### Security Mechanism
        - JWT
        - Spring Security
- Field validation, error handling, and test coverage

